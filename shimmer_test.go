package main

import (
	"bufio"
	"bytes"
	"fmt"
	"net/mail"
	"os"
	"strings"
	"testing"
	"time"
)

func assertBool(t *testing.T, see bool, expect bool) {
	t.Helper()
	if see != expect {
		t.Errorf("assertBool: expected %v saw %v", expect, see)
	}
}

func assertTrue(t *testing.T, see bool) {
	t.Helper()
	assertBool(t, see, true)
}

func assertStringEqual(t *testing.T, a string, b string) {
	t.Helper()
	if a != b {
		t.Fatalf("assertStringEqual: expected %q != %q", a, b)
	}
}

func assertStringExpect(t *testing.T, e string, s string) {
	t.Helper()
	if e != s {
		t.Fatalf("assertEqual: expected %q, saw %q", e, s)
	}
}

func TestArmor(t *testing.T) {
	type testEntry struct {
		before string
		after  string
	}
	tests := []testEntry{
		{"aaa", "'aaa'"},
		{"b'c", "'b''c'"},
		{"e''f", "'e''''f'"},
	}
	for _, item := range tests {
		assertStringEqual(t, armor(item.before), item.after)
	}
}

func TestReadProcess(t *testing.T) {
	ofp, cmd, err1 := readFromProcess("echo 'Hello, world!'")
	if err1 != nil {
		t.Fail()
	}
	defer ofp.Close()
	r := bufio.NewReader(ofp)
	line, _ := r.ReadString(byte('\n'))
	cmd.Wait()
	assertStringExpect(t, "Hello, world!\n", line)
}

var here string

func RepositorySetup(t *testing.T, stem string, from string) (*VCS, string, string) {
	t.Helper()
	here, err := os.Getwd()
	if err != nil {
		t.Errorf("Getwd: %w", err)
	}
	reponame := fmt.Sprintf(stem+"-%s%d", rfc3339(time.Now()), os.Getpid())

	var maker string
	if from == "" {
		maker = "git init -q " + reponame
	} else {
		maker = "git clone -q " + from + " " + reponame
	}

	output, err := captureFromProcess(maker)
	if err != nil {
		t.Errorf("test repository creation: %s (%q)", err, output)
	}

	err = os.Chdir(reponame)
	if err != nil {
		t.Errorf("repository chdir: %s", err)
	}

	_, err = captureFromProcess("git config user.name 'Fred Foonly'")
	if err != nil {
		t.Errorf("configuring user name: %s", err)
	}

	_, err = captureFromProcess("git config user.email 'fred@foonly.com'")
	if err != nil {
		t.Errorf("configuring user email: %s", err)
	}

	repo := newVCS("example.com", 0, 100)
	if repo == nil {
		t.Errorf("%s is unexpectely not a directory", reponame)
	}
	_, err = captureFromProcess("git config --add shimmer.mta cat")
	if err != nil {
		t.Errorf("configuring mta: %s", err)
	}

	return repo, reponame, here
}

func RepositoryCleanup(t *testing.T, reponame string, here string) {
	t.Helper()
	os.Chdir(here)
	if err := os.RemoveAll(reponame); err != nil {
		t.Errorf("directory cleanup: %s", err)
	}
}

func makeCommit(t *testing.T, committer string, name string, content string, comment string) {
	var err error

	c, err := mail.ParseAddress(committer)
	if err != nil {
		t.Errorf("invalid committer address: %s", err)
	}

	exists := func(pathname string) bool {
		_, err := os.Stat(pathname)
		return !os.IsNotExist(err)
	}

	doAdd := !exists(name)

	file, err := os.OpenFile(name, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		t.Errorf("OpenFile in makeCommit: %s", err)
	}
	file.WriteString(content)
	file.Close()

	if doAdd {
		_, err = captureFromProcess("git add " + name)
		if err != nil {
			t.Errorf("add makeCommit: %s", err)
		}
	}

	os.Setenv("GIT_COMMITTER_NAME", c.Name)
	os.Setenv("GIT_COMMITTER_EMAIL", c.Address)
	_, err = captureFromProcess("git commit -q -m '" + comment + "' " + name)
	if err != nil {
		t.Errorf("running git commit: %s", err)
	}
}

func makeNote(t *testing.T, engine *Engine, user string, message string) parsedRequest {
	in := strings.NewReader(message)
	parsedUpdate, err := engine.requestParser(in, user)
	if err != nil {
		t.Errorf("Parse failure: %s", err)
	}
	_, err = engine.vcs.commitNote(parsedUpdate.dump())
	if err != nil {
		t.Errorf("Commit failure: %s", err)
	}
	return parsedUpdate
}

func captureCat(engine *Engine, filter string) string {
	buf := bytes.NewBuffer(nil)
	engine.cat(filter, buf)
	return buf.String()
}

func TestVCS(t *testing.T) {
	repo1, reponame, here := RepositorySetup(t, "alpha", "")
	defer RepositoryCleanup(t, reponame, here)

	if testing.Verbose() {
		repo1.verbose = logCOMMANDS
	}

	assertTrue(t, !repo1.hasBranches())

	fp, err := os.OpenFile(".git/config", os.O_RDWR|os.O_APPEND, 0666)
	if err != nil {
		t.Errorf("config file open failed: %s", err)
	}
	_, err = fp.WriteString("[shimmer]\n\tfoo = bar\n")
	if err != nil {
		t.Errorf("config file open failed: %s", err)
	}
	assertStringExpect(t, "bar\n", getProperty("shimmer.foo"))

	makeCommit(t, "Fred J. Foonly <fred@foonly.com>", "README", "Drink me\n", "Check that commit works before the engine stuff\n")

	assertTrue(t, repo1.hasBranches())
}

type engineHarness struct {
	engine   Engine
	name     string
	here     string
	camefrom string
}

func newHarness(t *testing.T, stem string, from string) *engineHarness {
	var harness engineHarness
	harness.engine.vcs, harness.name, harness.camefrom = RepositorySetup(t, stem, from)
	harness.here, _ = os.Getwd()
	if testing.Verbose() {
		harness.engine.vcs.verbose = logCOMMANDS
	}
	return &harness
}

func (harness *engineHarness) cleanup(t *testing.T) {
	RepositoryCleanup(t, harness.name, harness.camefrom)
}

func (harness *engineHarness) gointo(t *testing.T) {
	os.Chdir(harness.here)
}

const fred string = "Fred J. Foonly <fred@foonly.com>"
const wilma string = "Wilma J. Foonly <wilma@foonly.com>"

//const foozle = `"Foozle" <foozle@foozle.com>`

func TestEngine(t *testing.T) {
	apple := newHarness(t, "apple", "")
	defer apple.cleanup(t)

	// Notes won't work without an anchor commit
	makeCommit(t, fred, "README", "the quick brown fox\n", "First commit.\n")

	// Make a note
	const head1 = "Subject: Experimental thread-starter\n"
	const body1 = "APPLE1\n"
	makeNote(t, &apple.engine, fred, head1+"\n"+body1)

	// Test cat
	made := captureCat(&apple.engine, "")
	// Test is written in this way because a generated ID will have beem
	// inserted in the message as committed.
	if !strings.Contains(made, head1) || !strings.Contains(made, body1) {
		t.Errorf("new-message content is not as expected: %s", made)
	}

	// Test list
	ids, err := apple.engine.list("")
	if err != nil {
		t.Errorf("list failed: %s", err)
	}
	if len(ids) != 1 {
		t.Errorf("list length not as expected")
	} else if ids[0] != "issue:0001-01-01T00:01:40.000Z!fred@foonly.com" {
		t.Errorf("tag is not as expected: %q", ids[0])
	}

	// Make a second note
	const head2 = "Subject: Second experimental thread-starter\n"
	const body2 = "APPLE2\n"
	makeNote(t, &apple.engine, fred, head2+"\n"+body2)

	checklog := func(lookfor string, expect bool) {
		t.Helper()
		assertBool(t, strings.Contains(captureCat(&apple.engine, ""), lookfor), expect)
	}

	jumpto := func(harness *engineHarness) {
		t.Helper()
		if err := os.Chdir(harness.here); err != nil {
			t.Errorf("repository jump failed: %s", err)
		}
	}
	os.Chdir(apple.here)

	// Test that both notes are present in the log
	checklog(head1, true)
	checklog(head2, true)

	os.Chdir(apple.camefrom)
	orange := newHarness(t, "orange", apple.name)
	defer orange.cleanup(t)

	// Test that both notes are not present in the log of the
	// clone before sync
	checklog(head1, false)
	checklog(head2, false)

	sync := func(t *testing.T, harness *engineHarness) {
		t.Helper()
		if _, err := harness.engine.vcs.sync("origin"); err != nil {
			t.Errorf("sync failure at %s: %s", harness.name, err)
		}
	}

	sync(t, orange)

	// Test that both notes *are* not present in the log of the
	// clone after sync
	checklog(head1, true)
	checklog(head2, true)

	const head3 = "Subject: Third experimental thread-starter\n"
	const body3 = "ORANGE1\n"
	makeNote(t, &orange.engine, fred, head3+"\n"+body3)

	checklog(body3, true)

	sync(t, orange) // should push note 3 back to Applw

	jumpto(apple)

	checklog(head3, true) // check back-propagation of ORANGE1

	checklog(head2, true) // previous commit has to be OK too

	// Message and log changes that orange won't have

	const head4 = "Subject:  Fourth experimental thread-starter\n"
	const body4 = "APPLE4\n"
	makeNote(t, &apple.engine, fred, head4+"\n"+body4)

	checklog(body4, true) // Verify the note that just dropped

	jumpto(orange)

	_, err = captureFromProcess("git pull -q")
	if err != nil {
		t.Errorf("pull failed: %s", err)
	}

	// check non-propagation of notes by commit pull
	checklog(body4, false)

	sync(t, orange)

	// verify notes fetch from apple to banana
	checklog(body4, true)

	os.Chdir(apple.camefrom)

	// Now try to set up a scenario where the head note gets stomped on
	os.Chdir(apple.camefrom)
	banana := newHarness(t, "banana", apple.name)
	defer banana.cleanup(t)

	const head5 = "Subject:  Fifthth experimental thread-starter\n"
	const body5 = "BANANA1\n"
	makeNote(t, &apple.engine, fred, head5+"\n"+body5)

	jumpto(orange)

	const head6 = "Subject: Sixth experimental thread-starter\n"
	const body6 = "ORANGE2\n"
	makeNote(t, &orange.engine, fred, head6+"\n"+body6)

	sync(t, orange)

	jumpto(apple)

	checklog("ORANGE2", true)

	const head7 = "Subject: Seventh experimental thread-starter\n"
	const body7 = "APPLE5\n"
	makeNote(t, &apple.engine, fred, head7+"\n"+body7)

	jumpto(banana)

	sync(t, banana)

	checklog("APPLE5", true)

	sync(t, banana)

	jumpto(apple)

	checklog("ORANGE2", true)

	checklog("BANANA1", true)
}

func TestHeaderInheritance(t *testing.T) {
	parts := replacingHeaders.Union(preserveHeaders).Union(discardHeaders)
	order := orderedStringSet(headerOrder)
	if !parts.Equal(order) {
		t.Errorf("parts - order = %s, order - parts = %s", parts.Subtract(order), order.Subtract(parts))
	}

	apple := newHarness(t, "apple", "")
	defer apple.cleanup(t)

	// Notes won't work without an anchor commit
	makeCommit(t, fred, "README", "the quick brown fox\n", "First commit.\n")

	// Make a note
	const head1 = "Subject: Experimental thread-starter\nX-Assigned-To: " + fred + "\n"
	const body1 = "APPLE1\n"
	makeNote(t, &apple.engine, fred, head1+"\n"+body1)
	const firstid string = "issue:0001-01-01T00:01:40.000Z!fred@foonly.com"

	// Make a second note, check that it inherits X-Assigned-to
	const head2 = "Subject: Thread followup\nX-Message-Tags: " + firstid + "\nX-Visibility:hide\n"
	const body2 = "APPLE2\n"
	makeNote(t, &apple.engine, wilma, head2+"\n"+body2)

	history := captureCat(&apple.engine, "")
	os.Stdout.WriteString(history)
	if count := strings.Count(history, "X-Assigned-To"); count != 2 {
		t.Errorf("Replacing inheritance of X-Assigned-To failed, %d instances.", count)
	}

	// Make a third note without Subject, check that it inherits Subject
	const head3 = "X-Message-Tags: " + firstid + "\n"
	const body3 = "APPLE3\n"
	makeNote(t, &apple.engine, fred, head3+"\n"+body3)

	history = captureCat(&apple.engine, "")
	if count := strings.Count(history, "Subject: Thread followup"); count != 2 {
		t.Errorf("Replacing inheritance of Subject failed, %d instances.", count)
	}
	if count := strings.Count(history, "X-Visibility: hide"); count != 2 {
		t.Errorf("Replacing inheritance of X-Visibility, %d instances.", count)
	}

	assertStringExpect(t, fred+`, `+wilma,
		apple.engine.subscriptionList(firstid))

	// Test unsubscription
	const text4 = "X-Message-Tags: " + firstid + "\nX-Action: leave\n\nAPPLE4\n"
	req := makeNote(t, &apple.engine, `"Foozle" <foozle@foozle.com>`, text4)

	extracted := apple.engine.subscriptionList(firstid)
	assertStringExpect(t, `Fred J. Foonly <fred@foonly.com>, Wilma J. Foonly <wilma@foonly.com>`, extracted)

	// To check that sjipping to an MTA works, enable -v
	// ansee the message with oreoended To line going to stdout.
	if testing.Verbose() {
		req.pushNotify(extracted)
	}

	committers := apple.engine.vcs.getCommitters()
	if !committers.Equal(newOrderedStringSet(fred)) {
		t.Errorf("committer list is not as expected: %s", committers)
	}

	//os.Stdout.WriteString(history)
}

// end
