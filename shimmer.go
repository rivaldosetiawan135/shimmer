// SPDX-License-Identifier: BSD-2-Clause
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/mail"
	"os"
	"os/exec"
	"sort"
	"strings"
	"time"

	tcell "github.com/gdamore/tcell/v2"
	term "golang.org/x/term"
)

var verbose uint

// rfc3339 makes a UTC RFC3339 string with milliseconds from a system timestamp.
func rfc3339(t time.Time) string {
	return t.UTC().Format("2006-01-02T15:04:05.000Z")
}

// armor safes its argument so it can be passed to the shell without risk
func armor(s string) string {
	return "'" + strings.Replace(s, "'", "''", -1) + "'"
}

const (
	logSHOUT    uint = 1 << iota // Errors and urgent messages
	logWARN                      // Exceptional condition, probably not bug (default log level)
	logCOMMANDS                  // Show commands as they are executed
)

// logEnable is a hook to set up log-message filtering.
func logEnable(level uint) bool {
	return verbose >= level
}

func speak(msg string, args ...interface{}) {
	fmt.Printf("shimmer: "+msg+"\n", args...)
}

func croak(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "shimmer: "+msg+"\n", args...)
	os.Exit(1)
}

/* Bottom layer: manage slave processes */

func readFromProcess(command string) (io.ReadCloser, *exec.Cmd, error) {
	cmd := exec.Command("sh", "-c", command+" 2>&1")
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}
	if logEnable(logCOMMANDS) {
		speak("%s: reading from '%s'\n",
			rfc3339(time.Now()), command)
	}
	err = cmd.Start()
	if err != nil {
		return nil, nil, err
	}
	// Pass back cmd so we can call Wait on it and get the error status.
	return stdout, cmd, err
}

func lineByLine(command string, errfmt string, hook func(string) error) error {
	stdout, cmd, err1 := readFromProcess(command)
	if err1 != nil {
		return err1
	}
	defer stdout.Close()
	r := bufio.NewReader(stdout)
	for {
		line, err2 := r.ReadString(byte('\n'))
		if err2 == io.EOF {
			if cmd != nil {
				cmd.Wait()
			}
			break
		} else if err2 != nil {
			return fmt.Errorf(errfmt, err2)
		}
		// Ugh, wecan only pass out the last error
		err1 = hook(line)
	}
	return err1
}

func writeToProcess(command string) (io.WriteCloser, *exec.Cmd, error) {
	cmd := exec.Command("sh", "-c", command+" 2>&1")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	stdout, err := cmd.StdinPipe()
	if err != nil {
		return nil, nil, err
	}
	if logEnable(logCOMMANDS) {
		speak("%s: writing to '%s'\n",
			rfc3339(time.Now()), command)
	}
	err = cmd.Start()
	if err != nil {
		return nil, nil, err
	}
	// Pass back cmd so we can call Wait on it and get the error status.
	return stdout, cmd, err
}

// capture runs a specified command, capturing the output.
func captureFromProcess(command string) (string, error) {
	if logEnable(logCOMMANDS) {
		speak("%s: capturing %s", rfc3339(time.Now()), command)
	}
	cmd := exec.Command("sh", "-c", command)
	content, err := cmd.CombinedOutput()
	if logEnable(logCOMMANDS) {
		speak(string(content))
	}
	return string(content), err
}

/* Middle layer: communicate with the current repository */

// VCS encapsulates generation commands to the underlying VCS
type VCS struct {
	verbose   uint
	user      mail.Address
	testclock time.Time
}

func exists(pathname string) bool {
	_, err := os.Stat(pathname)
	return !os.IsNotExist(err)
}

func isdir(pathname string) bool {
	st, err := os.Stat(pathname)
	return err == nil && st.Mode().IsDir()
}

func newVCS(hostname string, verbose uint, clockbase int64) *VCS {
	var out VCS
	out.verbose = verbose

	if !exists(".git") || !isdir(".git") {
		return nil
	}

	out.user.Name = "shimmer"
	out.user.Address = hostname

	out1, err1 := exec.Command("git", "config", "user.name").CombinedOutput()
	out2, err2 := exec.Command("git", "config", "user.email").CombinedOutput()
	if err1 == nil && len(out1) != 0 && err2 == nil && len(out2) != 0 {
		out.user.Name = strings.Trim(string(out1), "\n")
		out.user.Address = strings.Trim(string(out2), "\n")
	}

	if clockbase > 0 {
		out.testclock = out.testclock.Add(time.Second * time.Duration(clockbase))
	}

	return &out
}

func (vcs *VCS) time() time.Time {
	if !vcs.testclock.IsZero() {
		return vcs.testclock
	}
	return time.Now()
}

func (vcs *VCS) userid() string {
	return strings.Replace(vcs.user.String(), `"`, ``, -1)
}

func (vcs *VCS) hasBranches() bool {
	stdout, _, err := readFromProcess("git branch")
	if err != nil {
		return false
	}
	b, err := ioutil.ReadAll(stdout)
	return err == nil && len(b) > 0
}

func (vcs *VCS) getCommitters() orderedStringSet {
	var committers orderedStringSet
	lineByLine("git log '--format=%cN <%cE>'", "git remote", func(line string) error {
		committers.Add(strings.TrimSpace(line))
		return nil
	})
	return committers
}

func (vcs *VCS) commitNote(text string) (string, error) {
	if !vcs.testclock.IsZero() {
		vcs.testclock = vcs.testclock.Add(time.Second * 10)
		os.Setenv("GIT_AUTHOR_DATE", rfc3339(vcs.testclock))
	}

	// Attaches the note to the current head object */
	return captureFromProcess("git notes --ref shimmer append -m '" + text + "'")
}

func (vcs *VCS) getNotes() (string, error) {
	stdout, cmd, err1 := readFromProcess("git log --reverse --notes=shimmer --format=%N")
	if err1 != nil {
		return "", err1
	}
	defer stdout.Close()
	r := bufio.NewReader(stdout)
	var out string
	for {
		line, err2 := r.ReadString(byte('\n'))
		if err2 == io.EOF {
			if cmd != nil {
				cmd.Wait()
			}
			break
		} else if err2 != nil {
			return out, fmt.Errorf("getLog: %s", err2)
		}
		out += line
	}
	return out, nil
}

func (vcs VCS) remotes() map[string]bool {
	remotes := make(map[string]bool)
	err := lineByLine("git remote", "git remote", func(line string) error {
		remotes[strings.TrimSpace(line)] = true
		return nil
	})
	if err != nil {
		return nil
	}
	return remotes
}

func (vcs VCS) sync(upstream string) (string, error) {
	output, err := captureFromProcess("git fetch -q " + upstream + " refs/notes/shimmer:refs/notes/" + upstream + "/shimmer")
	if err != nil {
		return output, fmt.Errorf("sync fetch: %s", err)
	}
	os.Setenv("GIT_NOTES_REF", "refs/notes/shimmer")
	output, err = captureFromProcess("git notes merge -q -s union " + upstream + "/shimmer")
	if err != nil {
		return output, fmt.Errorf("sync mergw: %s", err)
	}
	output, err = captureFromProcess("git push -q " + upstream + " refs/notes/shimmer")
	if err != nil {
		return output, fmt.Errorf("sync push: %s", err)
	}
	return output, nil
}

func getProperty(prop string) string {
	fp, _, _ := readFromProcess("git config " + prop)
	defer fp.Close()
	val, _ := ioutil.ReadAll(fp)
	return string(val)
}

/* Top layer: shimmer command logic */

type orderedStringSet []string

var nullOrderedStringSet orderedStringSet

func newOrderedStringSet(elements ...string) orderedStringSet {
	set := make([]string, 0)
	for _, el := range elements {
		found := false
		for _, already := range set {
			if already == el {
				found = true
			}
		}
		if !found {
			set = append(set, el)
		}
	}
	return set
}

// Listify bares the underlying map so we can iterate over its keys
func (s orderedStringSet) Listify() []string {
	return s
}

func (s orderedStringSet) Contains(item string) bool {
	for _, el := range s {
		if item == el {
			return true
		}
	}
	return false
}

func (s *orderedStringSet) Remove(item string) bool {
	for i, el := range *s {
		if item == el {
			// Zero out the deleted element so it's GCed
			copy((*s)[i:], (*s)[i+1:])
			(*s)[len(*s)-1] = ""
			*s = (*s)[:len(*s)-1]
			return true
		}
	}
	return false
}

func (s *orderedStringSet) Add(item string) {
	for _, el := range *s {
		if el == item {
			return
		}
	}
	*s = append(*s, item)
}

func (s orderedStringSet) Subtract(other orderedStringSet) orderedStringSet {
	var diff orderedStringSet
	for _, outer := range s {
		for _, inner := range other {
			if outer == inner {
				goto dontadd
			}
		}
		diff = append(diff, outer)
	dontadd:
	}
	return diff
}

func (s orderedStringSet) Intersection(other orderedStringSet) orderedStringSet {
	// Naive O(n**2) method - don't use on large sets if you care about speed
	var intersection orderedStringSet
	for _, item := range s {
		if other.Contains(item) {
			intersection = append(intersection, item)
		}
	}
	return intersection
}

func (s orderedStringSet) Union(other orderedStringSet) orderedStringSet {
	var union orderedStringSet
	union = s[:]
	for _, item := range other {
		if !s.Contains(item) {
			union = append(union, item)
		}
	}
	return union
}

func (s orderedStringSet) String() string {
	if len(s) == 0 {
		return "[]"
	}
	var rep strings.Builder
	rep.WriteByte('[')
	lastIdx := len(s) - 1
	for idx, el := range s {
		fmt.Fprintf(&rep, "\"%s\"", el)
		if idx != lastIdx {
			rep.WriteString(", ")
		}
	}
	rep.WriteByte(']')
	return rep.String()
}

func (s orderedStringSet) Equal(other orderedStringSet) bool {
	if len(s) != len(other) {
		return false
	}
	// Naive O(n**2) method - don't use on large sets if you care about speed
	for _, item := range s {
		if !other.Contains(item) {
			return false
		}
	}
	return true
}

func (s orderedStringSet) Empty() bool {
	return len(s) == 0
}

func setify(header string) orderedStringSet {
	var out orderedStringSet
	if header != "" {
		for _, token := range strings.Split(header, ",") {
			out.Add(strings.TrimSpace(token))
		}
	}
	return out
}

// Engine is a command interpreter primitves for the storage manager
type Engine struct {
	vcs *VCS
}

func addressesString(addresses []*mail.Address) string {
	out := ""
	for _, address := range addresses {
		out += strings.Replace(address.String(), `"`, ``, -1) + ", "
	}
	if strings.HasSuffix(out, ", ") {
		out = out[:len(out)-2]
	}
	return strings.Replace(out, `"`, ``, -1)
}

func (engine *Engine) generateActionStamp() string {
	if !engine.vcs.testclock.IsZero() {
		return rfc3339(engine.vcs.testclock) + "!" + engine.vcs.user.Address
	}
	return rfc3339(time.Now()) + "!" + engine.vcs.user.Address
}

type parsedRequest struct {
	from, threadTags string
	addressees       []*mail.Address
	mergeRequest     string
	headers, body    string
}

func (engine *Engine) requestParser(in io.Reader, user string) (parsedRequest, error) {
	var parsed parsedRequest
	var subject string

	if user == "" {
		user = engine.vcs.userid()
	}

	message, err := mail.ReadMessage(in)
	if err != nil {
		return parsed, err
	}

	header := message.Header
	stash := ""

	// Cumulates over its threads
	for _, hdr := range []string{"In-Reply-To", "References", "X-Assigned-To"} {
		if payload := header.Get(hdr); payload != "" {
			addresses, err := mail.ParseAddressList(payload)
			if err != nil {
				return parsed, fmt.Errorf("bad %s header in message input: %s", hdr, err)
			}
			stash += hdr + ": " + addressesString(addresses) + "\n"
		}
	}

	if subject = header.Get("Subject"); subject != "" {
		stash += "Subject: " + subject + "\n"
	}

	if parsed.threadTags = header.Get("X-Message-Tags"); parsed.threadTags != "" {
		stash += "X-Message-Tags: " + parsed.threadTags + "\n"
	}

	if visibility := header.Get("X-Visibility"); visibility != "" {
		stash += "X-Visibility: " + visibility + "\n"
	}

	if action := header.Get("X-Action"); action != "" {
		stash += "X-Action: " + action + " \n"
	}

	if parsed.mergeRequest = header.Get("X-Merge-Request"); parsed.mergeRequest != "" {
		stash += "X-Merge-Request: " + parsed.mergeRequest + "\n"
	}

	body, err := io.ReadAll(message.Body)
	if err != nil {
		return parsed, err
	}

	parsed.headers = stash
	parsed.body = string(body)

	prependHeader := func(header string, value string) {
		parsed.headers = header + ": " + value + "\n" + parsed.headers
	}
	appendHeader := func(header string, value string) {
		parsed.headers += header + ": " + value + "\n"
	}

	// We're using the standard RFC5322 date format here,  It carries
	// a risk if two messages with inheritable control headers arrive
	// in the same second, the wrong one could be applied to later
	// messages.
	prependHeader("Message-ID", "<"+engine.generateActionStamp()+">")
	prependHeader("Date", engine.vcs.time().Format(time.RFC1123Z))
	prependHeader("From", user)

	if parsed.threadTags == "" {
		var newHeader string
		if parsed.mergeRequest != "" {
			newHeader = "merge-request:" + engine.generateActionStamp()
		} else {
			newHeader = "issue:" + engine.generateActionStamp()
		}
		parsed.threadTags = newHeader
		appendHeader("X-Message-Tags", newHeader)
	}

	if assignees := message.Header.Get("X-Assigned-To"); assignees != "" {
		if !setify(assignees).Subtract(engine.vcs.getCommitters()).Empty() {
			return parsed, fmt.Errorf("assignees must be committers")
		}
	}

	return parsed, nil
}

func (update parsedRequest) dump() string {
	return update.headers + "\n" + update.body
}

func (update parsedRequest) pushNotify(subscribers string) error {
	mta := "sendmail -t"
	if configMTA := getProperty("shimmer.mta"); configMTA != "" {
		mta = configMTA
	}
	if envMTA := os.Getenv("SHIMMER_MTA"); envMTA != "" {
		mta = envMTA
	}
	wfp, cmd, err := writeToProcess(mta)
	if err != nil {
		return fmt.Errorf("pushNotify: %s", err)
	}
	wfp.Write([]byte("To: " + subscribers + "\n" + update.dump()))
	wfp.Close()
	cmd.Wait()

	return nil
}

// Next three lines span the complete set if headers interprerted by shimmer */
var preserveHeaders orderedStringSet = orderedStringSet{"From", "Date", "X-Action", "X-Message-Tags"}
var replacingHeaders orderedStringSet = orderedStringSet{
	"Subject",
	"X-Assigned-To",
	"X-Merge-Request",
	"X-Visibility",
}
var discardHeaders orderedStringSet = orderedStringSet{"In-Reply-To", "Message-ID", "References"}
var sensitiveHeaders orderedStringSet = orderedStringSet{"From", "X-Merge-Request", "X-Axtion"}
var headerOrder = []string{
	"From",
	"Date",
	"Message-ID",
	"Subject",
	"In-Reply-To",
	"References",
	"X-Message-Tags",
	"X-Visibility",
	"X-Assigned-To",
	"X-Action",
	"X-Merge-Request",
}

type datedMessage struct {
	text string
	tags string
	date time.Time
}

type shimmerState struct {
	messages     []datedMessage
	headersByTag map[string]mail.Header // This where we gather replacing headers
}

// loadShimmerState pulls all selected messages into memory.
// This is very conventient, but risks blowing up the working set
// to absurd size on large projects.  If this ever becomes in issue,
// reimplement so the internal structure stores promises - ways to
// retrive messages rather than the messages themselves.  No point
// in doing this until someone report an OOM, however.

func (engine *Engine) loadShimmerState(threadFilter string) (shimmerState, error) {
	var state shimmerState
	// The following lines control how a filter is matched
	// against X-Message-Tag lines.  This is just set
	// intersection, with both the filter and tag lines
	// interpreted as comma-separated tag lists. Success if the
	// filter is either empty (universal set) or there are common
	// tags in the lists.
	filter := setify(threadFilter)
	keepme := func(tags string) bool {
		return filter.Empty() || !filter.Intersection(setify(tags)).Empty()
	}

	var err error
	state.headersByTag = make(map[string]mail.Header)

	var msgBuffer, tags string
	var keep, headerLatch bool
	var date time.Time
	startmessage := func() {
		msgBuffer = ""
		keep = false
		headerLatch = false
		tags = ""
	}
	fromcount := 0
	endmessage := func() error {
		if fromcount > 0 && strings.TrimSpace(msgBuffer) != "" && (filter.Empty() || keep) {
			// Necessary because git log --format=%N generates extrainous
			// linefeeds for messages without notes.  The second \n goarantees
			// a spacer line after each message.
			msgBuffer = strings.TrimSpace(msgBuffer) + "\n\n"
			state.messages = append(state.messages, datedMessage{msgBuffer, tags, date})
			msgBuffer = ""
		}
		return nil
	}
	startmessage()
	catCommand := "git log --reverse --notes=shimmer --format=%N"
	err = lineByLine(catCommand, "git log: %s", func(line string) error {
		if strings.HasPrefix(line, "From: ") {
			fromcount++
			if err := endmessage(); err != nil {
				return err
			}
			startmessage()
		} else if !headerLatch {
			if strings.HasPrefix(line, "X-Message-Tags: ") {
				tags = strings.TrimSpace(line[15:])
				keep = keepme(tags)
			} else if strings.HasPrefix(line, "Date: ") {
				date, err = time.Parse(time.RFC1123Z, strings.TrimSpace(line[6:]))
				if err != nil {
					return err
				}
			} else if strings.TrimSpace(line) == "" {
				headerLatch = true
			}
		}
		msgBuffer += line
		return nil
	})
	if err == nil && fromcount > 0 {
		err = endmessage()
	}
	if err != nil {
		return state, fmt.Errorf("loadShimmerState failure: %s", err)
	}

	// Necessary because it's not guaranteed that messagews
	// arrived in time order
	sort.Slice(state.messages, func(i, j int) bool {
		return state.messages[i].date.Before(state.messages[j].date)
	})

	// Now that the messages are time-ordered, we can
	// ship then in date sequence, doing header inheritance
	for i, item := range state.messages {
		r := strings.NewReader(item.text)
		m, err := mail.ReadMessage(r)
		//fmt.Printf("Header seen: %s\n", m.Header)
		if err != nil {
			return state, err
		}

		body, err := io.ReadAll(m.Body)
		if err != nil {
			return state, err
		}

		// Use of Get means all headers adter the first with a given
		// header specifier are ignored

		tags := make([]string, 0)
		if tagline := m.Header.Get("X-Message-Tags"); len(tagline) > 0 {
			tags = setify(tagline)
		}

		//fmt.Printf("Header before: %s\n", m.Header)
		//fmt.Printf("Thread properties before: %s\n", headersByTag)

		out := ""
		for _, hd := range headerOrder {
			hdrval := m.Header.Get(hd)
			for _, tag := range tags {
				inherited := state.headersByTag[tag]
				//fmt.Printf("Inheriting %s from %s\n", hd, inherited)
				if len(inherited[hd]) > 0 {
					old := inherited[hd][0]
					if replacingHeaders.Contains(hd) && len(hdrval) == 0 {
						hdrval = old
					}
				}
			}
			if hdrval != "" {
				m.Header[hd] = []string{hdrval}
				out += hd + ": " + hdrval + "\n"
			}
		}
		out += "\n"
		out += string(body)

		// Copy header's directory keys and content, not just a
		// reference. This is so we guarantee can't screw ourselves
		// with side effects.  Might not be necessary but it's
		// cheap insurance.
		for _, tag := range tags {
			state.headersByTag[tag] = make(mail.Header)
			for k, v := range m.Header {
				state.headersByTag[tag][k] = v
			}
		}

		state.messages[i].text = out
	}

	return state, nil
}

type datedThread struct {
	tag   string
	began time.Time
}

func (state shimmerState) tagsByDate(all bool) []datedThread {
	taglist := make([]datedThread, 0)
	seen := newOrderedStringSet()
	// Messages are sorted by date
	for _, m := range state.messages {
		newtags := setify(m.tags).Subtract(seen)
		seen = seen.Union(newtags)
		if all || state.isVisible(newtags) {
			for _, tag := range newtags.Listify() {
				taglist = append(taglist, datedThread{tag, m.date})
			}
		}
	}
	return taglist
}

func (state shimmerState) isVisible(tags orderedStringSet) bool {
	// This tag set is visible if any tag in it is visible
	for _, tag := range tags.Listify() {
		hd, ok := state.headersByTag[tag]
		if !ok {
			return true
		}
		if hd.Get("X-Visibility") != "hide" {
			return true
		}
	}

	return false
}

func (engine *Engine) cat(threadFilter string, out io.Writer) error {
	state, err := engine.loadShimmerState(threadFilter)
	if err != nil {
		return fmt.Errorf("cat: %s", err)
	}
	for _, m := range state.messages {
		out.Write([]byte(m.text))
	}
	return nil
}

func (engine *Engine) list(tagFilter string) ([]string, error) {
	state, err := engine.loadShimmerState(tagFilter)
	if err != nil {
		return nil, fmt.Errorf("list: %s", err)
	}
	tagdict := make(map[string]bool)
	for _, m := range state.messages {
		for _, tag := range strings.Split(m.tags, ",") {
			tagdict[tag] = true
		}
	}
	if err != nil {
		return nil, err
	}
	names := make([]string, 0)
	for key := range tagdict {
		names = append(names, key)
	}
	sort.Strings(names)
	return names, err
}

func (engine *Engine) subscriptionList(threadFilter string) string {
	state, err := engine.loadShimmerState(threadFilter)
	if err != nil {
		return ""
	}
	list := newOrderedStringSet()
	for _, m := range state.messages {
		r := strings.NewReader(m.text)
		m, err := mail.ReadMessage(r)
		if err != nil {
			continue
		}

		// Assigned-To has to be processed early so it can be negated by From/X-Action
		addrList, err := mail.ParseAddressList(m.Header.Get("X-Assigned-To"))
		if err == nil {
			for _, addr := range addrList {
				list.Add(strings.Replace(addr.String(), `"`, ``, -1))
			}
		}

		from := m.Header.Get("From")
		action := m.Header.Get("X-Action")
		if strings.Contains(action, "leave") {
			list.Remove(from)
		} else {
			list.Add(from)
		}
	}
	return strings.Join(list.Listify(), ", ")
}

/* The browser */

func browse(engine *Engine, threadFilter string) {

	statusline := func(pwd string, modeString string, unhide bool) string {
		vis := ""
		if unhide {
			vis = " (all)"
		}
		return "---Shimmer: " + pwd + " " + modeString + vis + "-"
	}

	indexSummary := func(state shimmerState, thread datedThread, maxwidth int) string {
		return state.headersByTag[thread.tag].Get("Subject")
	}

	// No user-serviceable parts below this line

	if !term.IsTerminal(int(os.Stdin.Fd())) {
		croak("browse requires standard input to be a terminal.")
	}
	pwd, err := os.Getwd()
	if err != nil {
		croak("can't get current directory: %s", err)
	}

	fillThread := func(state shimmerState, threadTag string) []string {
		out := make([]string, 0)
		for _, m := range state.messages {
			//if setify(m.tags).Contains(threadTag) {
			out = append(out, strings.Split(m.text, "\n")...)
			//}
		}
		return out
	}

	var linewrap bool

	drawText := func(s tcell.Screen, x1, y1 int, style tcell.Style, text string) int {
		x2, y2 := s.Size()
		row := y1
		col := x1
		for _, r := range []rune(text) {
			s.SetContent(col, row, r, nil, style)
			col++
			if col >= x2 {
				if !linewrap {
					break
				}
				row++
				col = x1
			}
			if row > y2 {
				break
			}
		}
		return row
	}

	const helpText = `
Anywhere:

?         = display this command help
Ctrl-l    = force-refresh the display
Ctrl-c    = quit
q, Esc    = quit
q         = quit
v         = toggle visibility of hidden botes
s         = do a message synchronization with upstream
l         = reload message state
PgDn      = go to next screen-full of information
PgUp      = return to previous screen-full of information

Index page only:

Space     = visit thread
Enter     = vist thread
Up, p     = select previous thread
Down, n   = selectt next thread

Thread viewing only:

i      = go to index page
`

	// Initialize screen
	s, err := tcell.NewScreen()
	if err != nil {
		croak("TUI failed during screen allocation: %s", err)
	}

	if err := s.Init(); err != nil {
		croak("TUI failed during screen initialization: %s", err)
	}
	//s.EnableMouse()
	//s.EnablePaste()
	s.Clear()
	defer func() {
		s.Fini()
		os.Exit(0)
	}()

	defStyle := tcell.StyleDefault.Background(tcell.ColorReset).Foreground(tcell.ColorReset)
	statusStyle := tcell.StyleDefault.Foreground(tcell.ColorWhite).Background(tcell.ColorGray)
	selectStyle := defStyle.Reverse(true).Dim(true)
	s.SetStyle(defStyle)

	var atIndex bool
	var whichThread string
	var storeIndex int  // Index of first line of backing store displayed
	var selectIndex int // Index of selected item in backing store
	var modeString string
	var helpLine string
	var mainWindowSize int
	var backingStore, errorStore []string
	var unhide bool

	bumpSelect := func(incr int) {
		// First step: Ensure the selection index is valid
		selectIndex += incr
		if incr > 0 {
			if selectIndex >= len(backingStore)-1 {
				selectIndex = len(backingStore) - 1
			}
		}
		if incr < 0 {
			if selectIndex < 0 {
				selectIndex = 0
			}
		}
		// Second step: If this motion of the selector bar would
		// take it off-screen, change storeIndex until it's
		// still visible; this scrolls the display
		if selectIndex < storeIndex {
			storeIndex = selectIndex
		}
		if selectIndex >= storeIndex+mainWindowSize {
			storeIndex = selectIndex
		}
	}

	// Queue up the commands to load Shimmer state start at the index page
	s.PostEvent(tcell.NewEventKey(tcell.KeyRune, rune('l'), 0))
	s.PostEvent(tcell.NewEventKey(tcell.KeyRune, rune('i'), 0))

	var state shimmerState

	// Event loop
	for {
		// Poll event
		ev := s.PollEvent()

		xmax, ymax := s.Size()

		postError := func(errorText string) {
			errorStore = strings.Split(strings.TrimSpace(errorText), "\n")
			mainWindowSize = ymax - 2 - len(errorStore)
		}
		clearError := func() {
			errorStore = nil
			mainWindowSize = ymax - 2
		}

		clearError()

		nuke := false

		// Process event
		switch ev := ev.(type) {
		case *tcell.EventResize:
			xmax, ymax = s.Size()
			nuke = true
		case *tcell.EventKey:
			if k := ev.Key(); k == tcell.KeyEscape || k == tcell.KeyCtrlC || ev.Rune() == 'q' {
				return
			} else if k == tcell.KeyCtrlL {
				nuke = true
			} else if ev.Rune() == 'l' {
				state, err = engine.loadShimmerState(threadFilter)
				if err != nil {
					postError(fmt.Sprintf("TUI failed during shimmer state load: %s", err))
				}
			} else if ev.Rune() == 'i' {
				if !atIndex {
					linewrap = false
					storeIndex = 0
					modeString = "[Thread index]"
					helpLine = "q:Quit r:Reply u:Unsubscribe v:Visibility SPC:Select"
					atIndex = true
					backingStore = make([]string, 0)
					for _, thread := range state.tagsByDate(unhide) {
						backingStore = append(backingStore, indexSummary(state, thread, xmax))
					}
				}
			} else if ev.Rune() == 'v' {
				unhide = !unhide
			} else if ev.Rune() == 's' {
				out, err := engine.vcs.sync("origin")
				if err == nil {
					postError("sync successful")
					state, err = engine.loadShimmerState(threadFilter)
					if err != nil {
						postError(fmt.Sprintf("TUI failed during shimmer state reload: %s", err))
					}
				} else {
					postError(out)
				}
			} else if ev.Rune() == ' ' || ev.Rune() == '\n' {
				if atIndex {
					whichThread = backingStore[selectIndex][:10] // FIXME
					linewrap = true
					backingStore = fillThread(state, whichThread)
					storeIndex = 0
					modeString = whichThread
					helpLine = "q:Quit r:Reply u:Unsubscribe v:Visibility i:Index"
					atIndex = false
				}
			} else if ev.Key() == tcell.KeyUp || ev.Key() == 'p' {
				if atIndex {
					bumpSelect(-1)
				} else {
					bumpSelect(-(mainWindowSize))
				}
			} else if ev.Key() == tcell.KeyDown || ev.Key() == 'n' {
				if atIndex {
					bumpSelect(1)
				} else {
					bumpSelect(mainWindowSize)
				}
			} else if ev.Key() == tcell.KeyPgUp {
				bumpSelect(-mainWindowSize)
			} else if ev.Key() == tcell.KeyPgDn {
				bumpSelect(mainWindowSize)
			} else if ev.Rune() == '?' {
				postError(helpText)
			}
		}

		fillRight := func(text string, filler string) string {
			return text + strings.Repeat(filler, xmax-len(text))
		}

		// Repaint the screen buffer
		drawText(s, 0, 0, statusStyle, fillRight(helpLine, " "))
		lastline := 0
		for i := 0; i < mainWindowSize; i++ {
			if storeIndex+i < len(backingStore) {
				line := fillRight(backingStore[storeIndex+i], " ")
				if atIndex && (i == selectIndex-storeIndex) {
					lastline = drawText(s, 0, i+1, selectStyle, line)
				} else {
					lastline = drawText(s, 0, i+1, defStyle, line)
				}
			} else {
				lastline = drawText(s, 0, i+1, defStyle, fillRight("", " "))
			}
			if lastline >= mainWindowSize {
				break
			}
		}
		status := statusline(pwd, modeString, unhide)
		drawText(s, 0, mainWindowSize+1, statusStyle, fillRight(status, string([]byte{status[0]})))

		// And the transient error buffer
		for k, line := range errorStore {
			drawText(s, 0, mainWindowSize+2+k, defStyle, fillRight(line, " "))
		}

		// Update screen
		if nuke {
			s.Sync()
		} else {
			s.Show()
		}
	}
}

/* The main event */

func main() {
	var nomail bool
	var clockbase int64

	flag.BoolVar(&nomail, "n", false, "suppress notification sends")
	flag.UintVar(&verbose, "v", 0, "set verbosity level")
	flag.Int64Var(&clockbase, "T", 0, "clock base for developer test mode")
	flag.Parse()

	var command string
	var arguments []string

	if flag.NArg() == 0 {
		command = "browse"
		arguments = make([]string, 0)
	} else {
		command = flag.Arg(0)
		arguments = flag.Args()[1:]
	}

	hostname, err := os.Hostname()
	if err != nil {
		croak("hostname lookup failure: %s.", err)
	}

	vcs := newVCS(hostname, verbose, clockbase)
	if vcs == nil {
		croak("current directory doesn't look like a Git repository.")
	}

	if !vcs.hasBranches() {
		croak("can't operate on an empty repository")
	}

	var engine Engine
	engine.vcs = vcs

	threadFilter := strings.TrimSpace(strings.Join(arguments, " "))

	switch command {
	case "compose":
		fallthrough
	case "co":
		var source *os.File = os.Stdin

		if editor := os.Getenv("EDITOR"); editor != "" && term.IsTerminal(int(os.Stdin.Fd())) {
			source, err := ioutil.TempFile("/tmp", "shimmer")
			if err != nil {
				croak("preparing to edit: %s", err)
			}
			defer os.Remove(source.Name())
			source.WriteString("Subject: \n\n")
			_, err = captureFromProcess(editor + " " + source.Name())
			if err != nil {
				croak("editing new message: %s", err)
			}
		}

		parsedUpdate, err := engine.requestParser(source, "")
		if err != nil {
			croak("compose failed: %s", err)
		} else {
			_, err = engine.vcs.commitNote(parsedUpdate.dump())
			if err != nil {
				croak("compose failed: %s", err)
			} else if parsedUpdate.threadTags != "" {
				os.Stdout.WriteString(parsedUpdate.threadTags + "\n")
			}
			if !nomail {
				parsedUpdate.pushNotify(engine.subscriptionList(parsedUpdate.threadTags))
			}
		}
	case "sync":
		var upstream = "origin"
		if len(arguments) > 0 {
			upstream = arguments[0]
		}
		if !vcs.remotes()[upstream] {
			croak("no such remote as %s", upstream)
		}
		if _, err = vcs.sync(upstream); err != nil {
			croak("sync failure: %s", err)
		}
	case "cat":
		engine.cat(threadFilter, os.Stdout)
	case "list":
		if threads, err := engine.list(threadFilter); err == nil {
			for _, thread := range threads {
				os.Stdout.WriteString(thread + "\n")
			}
		} else {
			croak("list failure: %s", err)
		}

	case "browse":
		browse(&engine, threadFilter)
	case "help":
		os.Stdout.WriteString("shimmer [-n] [-v] [help | co[mpose] |cat [filter...] | list [filter...] | browse [filter...]]\n\n")
		flag.PrintDefaults()
		os.Stdout.WriteString(`
help: shows this message

sync: exchange message traffic with a peer repository, "origin" by default.

compose: take a message thread update on stdin in RFC5322 format

cat: dump selected message threads in RFC5322 format

list: list the IDs of selected threads

browse: run a TUI to browse selected threads (default)
`)
	default:
		croak("unknown command verb '" + command + "'; try 'shimmer help'.")
	}
}

// end
